# Week2Project #

This is our project for the 2nd week of CSC355: Open Source Development. This program uses a GUI that edits images with 
custom made "filters" made from the Python Imaging Library(Pillow). The editable photos are included in the "images" folder.

### Requirements ###

* [Install Python](https://www.python.org/downloads/)
* Numpy
```
pip install numpy
```
* Pillow
```
pip install Pillow
```


### Installation ###

Simply download the contents of this repository and run main.py.

### Authors ###

* Jack Castiglione
* Rahul Rangarajan
* Cameron King
* Nick Jonas

### Contributions ###

At this moment in time, we aren't taking any pull requests outside our group.

### License ###

This project was created by Jack Castiglione, Rahul Rangarajan, Cameron King and Nick Jonas. It is free software 
and may be redistributed under the terms specified in the [LICENSE](https://bitbucket.org/Breadstix/week2project/src/master/LICENSE) file.